<!DOCTYPE html>
  <html>
    <head>  
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
      <title><?php the_title(); ?></title>
	  <?php wp_head(); ?>
      <!-- Font -->
	  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
	 
      <!-- CSS  -->      
      <link href="<?php echo get_template_directory_uri();?>/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>


    </head>

    <body>