<header class="md-header">
    <div class="row">
        <div class="banner">
            <h2>L'instant Beauté</h2>
        </div>


        <a href="#" class="logo-header" style="visibility: visible;">

            <img src="<?php echo get_template_directory_uri(); ?>/img/logo-inverse.png" alt="logo" />
        </a>
        <div class="wp-menu">
            <nav class="nav-menu" style="visibility: visible;">

                <?php
                wp_nav_menu(
                    array(
                        'theme_location' => 'menu-header',
                        'menu_class' => 'topmenu'
                    )
                );

                ?>

                <a class="btn-ticketing" href="https://www.planity.com/linstant-beaute-43800-vorey" target="_blank" title="Prendre RDV">
                    Prendre RDV </a>
            </nav>
        </div>

        <div class="burger-menu">
            <p class="title-burger">L'instant Beauté</p>
            <div class="input-div">
                <input type="checkbox" class="input-burger" />
                <span></span>
                <span></span>
                <span></span>

                <?php
                wp_nav_menu(
                    array(
                        'theme_location' => 'menu-header',
                        'menu_class' => 'topmenu'
                    )
                );

                ?>
            </div>
        </div>


        <svg preserveAspectRatio="none" viewBox="0 0 1440 84" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <path fill="#E5E5E5" id="path0_fill-el" d="M 1440 83L 0 83L 0 0C 74.8333 17.8333 344.7 50 709.5 50C 1074.3 50 1338.83 17.8333 1440 0L 1440 83Z"></path>
        </svg>

    </div>
</header>