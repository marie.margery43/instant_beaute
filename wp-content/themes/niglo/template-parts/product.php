<div class="main-wp special-page">
  <section class="md-section section-content main-container">
    <div class="row">
      <div class="main-content">
        <h2><?php the_title(); ?></h2>

        <?php
        wp_nav_menu(
          array(
            'theme_location' => 'menu-sidebar',
            'menu_class' => 'topmenu'
          )
        );

        ?>

        <div class="product-grid">
          <?php
          /**
           * The template for instant B
           * 
           * @package Wordpress
           * @subpackage niglo
           * @since niglo 1
           */

          $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

          // Verifier l'activation de ACF
          if (!function_exists('get_field')) return;
          ?>

          <?php $loop = new WP_Query(array('post_type' => 'soin', 'posts_per_page' => 9, 'paged' => $paged, 'order' => 'DESC')); ?>
          <?php while ($loop->have_posts()) : $loop->the_post(); ?>
          <div class="card-container">
          <div class="card">
              <?php the_content(); ?>
            
          </div>

        </div>
            
          <?php endwhile; ?>
        </div>
        <div class="pagination">
          <?php next_posts_link('Page Suivante', $loop->max_num_pages);
                previous_posts_link('Page Précédente');
                wp_reset_postdata();
          ?>
        </div>

      </div>
    </div>
  </section>
</div>