<footer class="md-footer">
	<div class="row">
		<div class="wp-footer">
			<div class="row-extra row-up">
				<div class="footer-column column-logo">
					<a href="#" class="logo">
						<img src="<?php echo get_template_directory_uri(); ?>/img/logo_transparent.png" alt="logo" class="logo" />
					</a>
					<div class="container">
						<div class="list-social">
							<a href="https://www.facebook.com/pages/category/Beauty--Cosmetic---Personal-Care/Linstant-Beaut%C3%A9-112421587059073/" class="logo-fb" target="_blank" title="Facebook">
								<i class="fab fa-facebook-square"></i>
							</a>
						</div>
						<a class="vector" href="https://fr.vecteezy.com/vecteur-libre/motif-floral">Motif Floral Vecteurs par Vecteezy</a>
						<a class="vector" href='https://fr.freepik.com/vecteurs/fond'>Fond vecteur créé par pikisuperstar - fr.freepik.com</a>
					</div>
				</div>
				<div class="footer-column column-links">
					<h2>
						Horaires </h2>
					<div class="container">
					<p>Mardi : 09:00 18:15</br>
					Mercredi : 09:00 12:00 </br>
					Jeudi : 09:00 18:00</br>
					Vendredi : 09:00 18:15</br>
					Samedi : 09:00 16:00</p>
					</div>
				</div>
				<div class="footer-column column-contact">
					<div class="md-contact">
						<h2>
							Coordonnées </h2>
						<div class="container">
							<div class="info">
								<p>2 rue Louis Jouvet</p>
								<p>43800 VOREY</p>
								<p>Téléphone : 0471013417</p>
								<a class="btn-default" href="contact" title="Contactez-nous">
									Contactez-nous </a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="wp-footer-down">
			<div class="row-extra">
				<div class="small-12 columns">
					<span class="copy-right">
						© 2021 - Marie MARGERY </span>
				</div>
			</div>
		</div>
	</div>
	<span class="check-menu"></span>
</footer>






<!--
 <footer id="footer" role="contentinfo">
   
   <div class="footer-inner">
     <p class="design-info">2021 - Designed By Marie Margery</p>
   </div>
 </footer>!-->