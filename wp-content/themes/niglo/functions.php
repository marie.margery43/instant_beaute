<?php 

function mes_menus() {
  register_nav_menus(
   array(
         'menu-header' => __( 'Menu Haut' ),
         'menu-sidebar' => __('Menu latéral'),
         
   )
  );
  add_theme_support( 'post-thumbnails' );
 }
 add_action( 'init', 'mes_menus' );

 
?>

<?php
add_filter( 'body_class', 'custom_body_class' );
/**
* Add custom field body class(es) to the body classes.
*
* It accepts values from a per-page custom field, and only outputs when viewing a singular static Page.
*
* @param array $classes Existing body classes.
* @return array Amended body classes.
*/
function custom_body_class( array $classes ) {
$new_class = is_page() ? get_post_meta( get_the_ID(), 'body_class', true ) : null;
if ( $new_class ) {
$classes[] = $new_class;
}
return $classes;
}
?>