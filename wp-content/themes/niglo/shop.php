

<?php
/*
 * Template Name: Products
 * description: >- Page template for shop
 */?>
<main class="main">
    <?php
    /**
     * The template for shop
     * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
     * @package WordPress
     * @subpackage Niglo
     * @since 1.0.0
     */
    get_header();
    get_template_part('template-parts/header');
    get_template_part('template-parts/product');
    get_template_part('template-parts/footer');
    get_footer();?>
</main>

