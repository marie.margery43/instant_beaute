<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'instant_beaute' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ' ,u>^>>K;taVU-R9;q uC~2`@`[VkY^768{K}Puf[C(Fb,uzvxahZ`aEd2vss!T5' );
define( 'SECURE_AUTH_KEY',  'tQ~:k6~>Ov)ejXvH5SyFM+)YHq;NZcVeQ5)gNI5-GW;Z;DU*WmTB>qIW9/)?J )[' );
define( 'LOGGED_IN_KEY',    '6Z0PMOT7-wG62,Vfrr.#k9-3QaR GaeS=bIF#J?{ZFAkx257)mv*cG,a|K~C<GN]' );
define( 'NONCE_KEY',        'dhD|;TJF_kxXcA9ix2wa9qkw]Q(+OtKkN>5SnSO[Z#04|jHH[3u6G367@{A%%zh6' );
define( 'AUTH_SALT',        'R+jLs=?Y/7P-sy.&n&tLj<l,j!-$ztDmfP!A{9w5tbK/P<j=6#_af|NzJlsY^;1G' );
define( 'SECURE_AUTH_SALT', 'X`=`)kUZ~kaqx&{QmEcry@F8P{IE=9rDq!={v;wa<FJO6+/sG IbEi~I[8*+wI4E' );
define( 'LOGGED_IN_SALT',   '~CW@gS?ATRK{-Kq<2Twfb=qlu2d/9}M(%&iC&]inb?/8$ucYsY#PyRh?(Sx5moL3' );
define( 'NONCE_SALT',       'vwQFd@#@Sr|x4 [1G:y[8.!}F`BW8q0(BJPOX_bJn-1d.SCA3M4/zDac#eE]Fiwc' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );


/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
